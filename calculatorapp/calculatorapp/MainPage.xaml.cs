﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
/// <summary>
/// Name: Khoa Mai
/// Class 481 - Spring 2020
/// Homework 2 - Calculator
/// Description: Simple mobile calculator
/// Note: User has to click CE after doing a calculation. For example, do 5+9, then press Equal. After that, user needs to 
/// click CE or C in order to do another calculation.
/// Currently, the calculator is limited to interger calculation only.
/// </summary>


namespace calculatorapp
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        int result = 0;
        string operand = "";
        string operat;
        public MainPage()
        {
            InitializeComponent();
        }

        void ButtonClicked(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button.Text == "1")
            {
                operand += "1";
                resultText.Text = operand.ToString(); }
            else if (button.Text == "2")
            { operand += "2";
                resultText.Text = operand.ToString(); }
            else if (button.Text == "3")
            { operand += "3";
                resultText.Text = operand.ToString(); }
            else if (button.Text == "4")
            { operand += "4";
                resultText.Text = operand.ToString(); }
            else if (button.Text == "5")
            { operand += "5";
                resultText.Text = operand.ToString(); }
            else if (button.Text == "6")
            { operand += "6";
                resultText.Text = operand.ToString(); }
            else if (button.Text == "7")
            { operand += "7";
                resultText.Text = operand.ToString(); }
            else if (button.Text == "8")
            { operand += "8";
                resultText.Text = operand.ToString(); }
            else if (button.Text == "9")
            { operand += "9";
                resultText.Text = operand.ToString(); }

            if (button.Text == "+")
            {
                operat = "+";
                resultText.Text = operat;
                operand += "+";
            }
            else if (button.Text == "-")
            {
                operat = "-";
                resultText.Text = operat;
                operand += "-";
            }
            else if (button.Text == "X")
            {
                operat = "x";
                resultText.Text = operat;
                operand += "x";          
            }
            else if (button.Text == "/")
            {
                operat = "/";
                resultText.Text = operat;
                operand += "/";
            }
            }
        void EqualClicked(object sender, EventArgs e)
        {
            if (operat == "+")
                result = (operand[0] -'0') + (operand[2] - '0');
            else if (operat == "-")
                result = (operand[0] - '0') - (operand[2] - '0');
            else if (operat == "x")
                result = (operand[0] - '0') * (operand[2] - '0');
            else if (operat == "/")
                result = (operand[0] - '0') / (operand[2] - '0');

            System.Console.WriteLine("First operand = " + (operand[0] - '0'));
            System.Console.WriteLine("Second operand = " + (operand[2] - '0'));
            System.Console.WriteLine("Operator = " + operat);
            System.Console.WriteLine("Result = " + result);



            this.resultText.Text = result.ToString();
        }

        void ClearClicked(object sender, EventArgs e)
        {
            operand = "";
            result = 0;
            this.resultText.Text = "0";
        }

    }
}
